interface Question {
  question: string;
  choices: string[];
  correctAnswer: number;
}

const questions: Question[] = [
  {
    question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
    choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
    correctAnswer: 0,
  },
  {
    question: 'Which method is used to add one or more elements to the end of an array?',
    choices: ['push()', 'join()', 'slice()', 'concat()'],
    correctAnswer: 0,
  },
  {
    question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
    choices: ['"327"', '"12"', '"57"', '"NaN"'],
    correctAnswer: 2,
  },
  {
    question: 'What is the purpose of the "use strict" directive in JavaScript?',
    choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
    correctAnswer: 2,
  },
  {
    question: 'What is the scope of a variable declared with the "let" keyword?',
    choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
    correctAnswer: 2,
  },
  {
    question: 'Which higher-order function is used to transform elements of an array into a single value?',
    choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
    correctAnswer: 2,
  },
  {
    question: 'What does the "=== " operator in JavaScript check for?',
    choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
    correctAnswer: 1,
  },
  {
    question: 'What is the purpose of the "this" keyword in JavaScript?',
    choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
    correctAnswer: 3,
  },
  {
    question: 'What does the "NaN" value represent in JavaScript?',
    choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
    correctAnswer: 0,
  },
  {
    question: 'Which method is used to remove the last element from an array?',
    choices: ['pop()', 'shift()', 'slice()', 'splice()'],
    correctAnswer: 0,
  },
];

// const mainDiv = document.getElementById('exercise2') as HTMLDivElement
// const scoreTxt = document.createElement('div')
// let score = 0
// scoreTxt.innerText = "Current Score: " + score + "  /10"
// mainDiv.appendChild(scoreTxt)

// function updateScore() {
//   score += 1
//   scoreTxt.innerText = "Current Score: " + score + "  /10"
// }

// questions.forEach(question => {
//   const divMainQuestion = document.createElement('div')
//   const paragraphQuestion = document.createElement('p')
//   let txt = document.createTextNode(question.question)
//   paragraphQuestion.appendChild(txt)
//   const divSectionRadioButton = document.createElement('div')
//   const divChoice = document.createElement('div')

//   question.choices.forEach(choice => {
//     const divChoice = document.createElement('div')
//     var radioButton = document.createElement("INPUT");
//     radioButton.setAttribute("type", "radio");
//     divChoice.appendChild(radioButton)
//     const spanTxt = document.createElement('span')
//     let txt = document.createTextNode(choice)
//     spanTxt.appendChild(txt)
//     divChoice.appendChild(spanTxt)
//     divSectionRadioButton.appendChild(divChoice)
//   })
//   var button = document.createElement("BUTTON");
//   var textButton = document.createTextNode("Submit");
//   const statustxt = document.createElement('p')
//   button.appendChild(textButton);
//   document.body.appendChild(button);
//   divMainQuestion.appendChild(paragraphQuestion)
//   divMainQuestion.appendChild(divSectionRadioButton)
//   divMainQuestion.appendChild(button)
//   divMainQuestion.appendChild(statustxt)
//   mainDiv.appendChild(divMainQuestion)
// })


// ...
const mainDiv = document.getElementById('exercise2') as HTMLDivElement;
const scoreTxt = document.createElement('div');
let score = 0;
scoreTxt.innerText = "Current Score: " + score + "  /10";
mainDiv.appendChild(scoreTxt);

questions.forEach((question, questionIndex) => {
  const divMainQuestion = document.createElement('div');
  const paragraphQuestion = document.createElement('p');
  const txt = document.createTextNode(question.question);
  paragraphQuestion.appendChild(txt);
  divMainQuestion.appendChild(paragraphQuestion);
  
  const divSectionRadioButton = document.createElement('div');
  const divChoice = document.createElement('div');
  
  question.choices.forEach((choice, choiceIndex) => {
    const divChoice = document.createElement('div');
    const radioButton = document.createElement("input");
    radioButton.setAttribute("type", "radio");
    radioButton.setAttribute("name", "question" + questionIndex);
    radioButton.setAttribute("value", choiceIndex.toString());
    divChoice.appendChild(radioButton);
    
    const spanTxt = document.createElement('span');
    const txt = document.createTextNode(choice);
    spanTxt.appendChild(txt);
    divChoice.appendChild(spanTxt);
    divSectionRadioButton.appendChild(divChoice);
  });
  
  const button = document.createElement("button");
  const textButton = document.createTextNode("Submit");
  button.appendChild(textButton);
  
  const statusTxt = document.createElement('p');
  
  button.addEventListener('click', () => {
    const selectedAnswer = document.querySelector(`input[name="question${questionIndex}"]:checked`) as HTMLInputElement;;
    if (selectedAnswer === null) {
      alert("Please choose an answer first!");
      return;
    }
    
    const selectedAnswerIndex = parseInt(selectedAnswer.value);
    const correctAnswerIndex = question.correctAnswer;
    
    const radioButtons = document.querySelectorAll(`input[name="question${questionIndex}"]`) as NodeListOf<HTMLInputElement>; ;
    radioButtons.forEach(radioButton => {
      radioButton.disabled = true;
    });
    button.disabled = true;
    
    if (selectedAnswerIndex === correctAnswerIndex) {
      score++;
      scoreTxt.innerText = "Current Score: " + score + "  /10";
      statusTxt.innerText = "Correct!";
    } else {
      statusTxt.innerText = "Incorrect!";
    }
  });
  
  divMainQuestion.appendChild(divSectionRadioButton);
  divMainQuestion.appendChild(button);
  divMainQuestion.appendChild(statusTxt);
  mainDiv.appendChild(divMainQuestion);
});
